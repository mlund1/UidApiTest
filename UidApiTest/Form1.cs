﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Mail;
using System.Net;
using MsOutlook = Microsoft.Office.Interop.Outlook;
using System.IO.Ports;

namespace UidApiTest
{
    public partial class Form1 : Form
    {
        string comPort = "COM4";
        int Baudrate = 115200;
        public Form1()
        {
            
            InitializeComponent();
            this.Text = "Production UID Demo";
            modelTextBox.Text = "CR350";
            serialTextBox.Focus();
        }

        private async void getUidButton_Click(object sender, EventArgs e)
        {
            richTextBox1.Clear();
            serialTextBox.SelectAll();
            toolStripLabel1.Text = "Retrieving authentication token...";
            await Task.Delay(2000);
            API_ReturnData apiResult = await GetAuthToken();
            if(apiResult.result)
            {
                toolStripLabel1.Text = "Registering for device UID...";
                await Task.Delay(2000);
                apiResult = await GetDeviceUid(apiResult.responseString, modelTextBox.Text, serialTextBox.Text);
                if(apiResult.result)
                {
                    //return success
                    toolStripLabel1.Text = "Successfully retrieved a device UID.";
                    richTextBox1.Text = apiResult.requestString + "\r\n\r\n" + apiResult.responseString;
                    await WriteUidToLogger(apiResult.responseString);
                    toolStripLabel1.Text = apiResult.responseString;
                }
                else
                {
                    //return failure
                    toolStripLabel1.Text = "Failed to retreive UID.";
                    richTextBox1.Text = "Failed to retreive UID:\r\n" + apiResult.requestString + "\r\n\r\n" + apiResult.responseString;
                }
            }
            else
            {
                //return failure
                toolStripLabel1.Text = "Failed to Authenticate.";
                richTextBox1.Text = "Failed to Authenticate:\r\n" + apiResult.requestString + "\r\n\r\n" + apiResult.responseString;
            }
            
        }

        private async Task<API_ReturnData> GetAuthToken()
        {
            API_ReturnData apiResult = new API_ReturnData();
            apiResult.result = false;
            apiResult.requestString = "";
            apiResult.responseString = "";
            var client = new RestSharp.RestClient("http://auth.campbellcloud.xyz/realms/campbell-cloud/protocol/openid-connect/token");
            client.Timeout = -1;
            RestSharp.RestRequest request = new RestSharp.RestRequest(RestSharp.Method.POST);
            request.AddHeader("Authorization", "Basic Y2FtcGJlbGwtZGV2OnBhc3N3b3Jk");
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.AddParameter("grant_type", "password");
            request.AddParameter("client_id", "legacy-login");
            request.AddParameter("username", "webteam@campbellsci.com");
            request.AddParameter("password", "9YFW9D6e9bXLUzw9");
            RestSharp.IRestResponse response = client.Execute(request);

            apiResult.responseString = response.Content;
            apiResult.requestString = client.BaseUrl.ToString();
            foreach (var param in request.Parameters)
                apiResult.requestString = apiResult.requestString + "\r\n<br />" + param;

            (int, string) parseResult = await ParseResponse(apiResult.responseString);
            if (parseResult.Item1 == 1)
            {
                apiResult.result = true;
                apiResult.responseString = parseResult.Item2;
            }
            else if (parseResult.Item1 == -1)
                await SendEmail(apiResult.requestString, apiResult.responseString);
            return apiResult;
        }

        private async Task<API_ReturnData> GetDeviceUid(string authToken, string modelNum, string serialNum)
        {
            API_ReturnData apiResult = new API_ReturnData();
            apiResult.result = false;
            apiResult.requestString = "";
            apiResult.responseString = "";
            var client = new RestSharp.RestClient("https://api.campbellcloud.xyz/v3/campbell-cloud/master-devices/");
            client.Timeout = -1;
            var request = new RestSharp.RestRequest(RestSharp.Method.POST);
            request.AddHeader("Authorization", $"Bearer {authToken}");
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.AddParameter("model", $"{modelNum}");
            request.AddParameter("serial", $"{serialNum}");
            request.AddParameter("manufacturer", "Campbell Scientific, Inc.");
            request.AddParameter("office", "us");
            RestSharp.IRestResponse response = client.Execute(request);

            apiResult.responseString = response.Content;
            apiResult.requestString = client.BaseUrl.ToString();
            foreach (var param in request.Parameters)
                apiResult.requestString = apiResult.requestString + "\r\n<br />" + param;

            (int, string) parseResult = await ParseResponse(apiResult.responseString);
            if (parseResult.Item1 == 1)
            {
                apiResult.result = true;
                apiResult.responseString = parseResult.Item2;
            }
            else if (parseResult.Item1 == -1)
                await SendEmail(apiResult.requestString, apiResult.responseString);
            else
            {
                if(parseResult.Item2 == "409") // {"statusCode":409,"error":"Conflict","message":"Unable to create device."}
                    apiResult = await RetrieveExistingUid();
            }
            return apiResult;
        }

        private async Task<API_ReturnData> RetrieveExistingUid()
        {
            API_ReturnData apiResult = await GetAuthToken();
            if(apiResult.result)
            {
                var client = new RestSharp.RestClient($"https://api.campbellcloud.xyz/v3/campbell-cloud/master-devices/{modelTextBox.Text}/{serialTextBox.Text}");
                client.Timeout = -1;
                var request = new RestSharp.RestRequest(RestSharp.Method.GET);
                request.AddHeader("Authorization", $"Bearer {apiResult.responseString}");
                request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                request.AddParameter("grant_type", "password");
                request.AddParameter("client_id", "legacy-login");
                request.AddParameter("username", "webteam@campbellsci.com");
                request.AddParameter("password", "9YFW9D6e9bXLUzw9");
                RestSharp.IRestResponse response = client.Execute(request);

                apiResult.responseString = response.Content;
                apiResult.requestString = client.BaseUrl.ToString();
                foreach (var param in request.Parameters)
                    apiResult.requestString = apiResult.requestString + "\r\n<br />" + param;

                (int, string) parseResult = await ParseResponse(apiResult.responseString);
                if (parseResult.Item1 == 1)
                {
                    apiResult.result = true;
                    apiResult.responseString = parseResult.Item2;
                }
                else if (parseResult.Item1 == -1)
                    await SendEmail(apiResult.requestString, apiResult.responseString);
            }
            return apiResult;
        }

        private async Task<(int, string)> ParseResponse(string responseString)
        {
            await Task.Delay(250);
            (int, string) result = (-1, "Unexpected response");
            if (responseString.Contains("\"statusCode\":"))
            {
                responseString = responseString.Substring(responseString.IndexOf("\"statusCode\":") + 13);
                responseString = responseString.Remove(responseString.IndexOf(",\""));

                switch (responseString)
                {
                    case "401": // {"statusCode":401,"error":"Unauthorized","message":"Missing authentication"}
                    case "400": // {"statusCode":400,"error":"Bad Request","message":"\"model\" is not allowed to be empty"} also applies to serial
                    case "404": //{ "statusCode":404,"error":"Not Found","message":"Not Found"}
                    case "409": // {"statusCode":409,"error":"Conflict","message":"Unable to create device."}
                    case "500": // status code 500 - internal server error or other network issue
                        result = (0, responseString);
                        break;
                    default:
                        break;
                }
            }
            else if (responseString.Contains("\"id\":\""))
            {
                responseString = responseString.Substring(responseString.IndexOf("\"id\":\"") + 6);
                responseString = responseString.Remove(responseString.IndexOf("\""));
                result = (1, responseString);
            }
            else if (responseString.Contains("access_token"))
            {
                responseString = responseString.Substring(responseString.IndexOf("access_token") + 12);
                responseString = responseString.Substring(responseString.IndexOf(":\"") + 2);
                responseString = responseString.Remove(responseString.IndexOf("\","));
                result = (1, responseString);
            }
            richTextBox1.AppendText("\r\n\r\n" + responseString);
            return result;
        }

        private async Task SendEmail(string requestString, string responseString)
        {
            try
            {
                await Task.Delay(250);
                MailMessage message = new MailMessage();
                SmtpClient smtp = new SmtpClient();
                message.From = new MailAddress("csi.testeng@gmail.com");
                message.To.Add(new MailAddress("mlund@campbellsci.com"));
                message.Subject = "Unexpected Response from UID API Exchange";
                message.IsBodyHtml = true; //to make message body as html  
                message.Body = "Request:<br />" + requestString + "<br /><br />Response:<br />" + responseString;
                smtp.Port = 587;
                smtp.Host = "smtp.gmail.com"; //for gmail host  
                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential("csi.testeng@gmail.com", "testengineering@csi");
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.Send(message);
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void outlookEmailButton_Click(object sender, EventArgs e)
        {
            try
            {
                // create the outlook application.
                MsOutlook.Application outlookApp = new MsOutlook.Application();
                if (outlookApp == null)
                    throw new Exception("outlookApp == null");

                // create a new mail item.
                MsOutlook.MailItem mail = (MsOutlook.MailItem)outlookApp.CreateItem(MsOutlook.OlItemType.olMailItem);

                // set html body. 
                // add the body of the email
                mail.HTMLBody = "email body";

                //Add attachments.
                //if (filePaths != null)
                //{
                //    foreach (string file in filePaths)
                //    {
                //        //attach the file
                //        MsOutlook.Attachment oAttach = mail.Attachments.Add(file);
                //    }
                //}

                //mail.Sender = "";
                mail.Subject = "subject";
                mail.To = "mckaylund@gmail.com";
                mail.Send();

                //if (sendType == MailSendType.SendDirect)
                //    mail.Send();
                //else if (sendType == MailSendType.ShowModal)
                //    mail.Display(true);
                //else if (sendType == MailSendType.ShowModeless)
                //    mail.Display(false);

                mail = null;
                outlookApp = null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private async Task<bool> WriteUidToLogger(string uid)
        {
            toolStripLabel1.Text = "Saving UID to device...";
            var hidInfo = await GetDeviceHID();
            if (!hidInfo.Item1)
            {
                richTextBox1.AppendText("\r\n\r\nUnable to retrieve device HID");
                return false;
            }

            richTextBox1.AppendText("\r\n\r\nPasscode: " + hidInfo.Item2);
            var tempFactResult = await SetFactoryMode(hidInfo.Item2, uid);
            if (!tempFactResult.Item1)
            {
                richTextBox1.AppendText($"\r\n\r\nProblem in SetFactoryMode: {tempFactResult.Item2}");
                return false;
            }

            toolStripLabel1.Text = "UID has been saved.";
            await Task.Delay(2000);
            toolStripLabel1.Text = "Printing UID Label...";
            await PrintLabel(modelTextBox.Text, serialTextBox.Text, uid);
            toolStripLabel1.Text = "Label printed.";
            await Task.Delay(2000);

            return true;
        }

        private async Task<(bool, string)> GetDeviceHID()
        {
            (bool, string) result = (false, "Untested"); //intialize to some recognizable and obviously wrong values
            Dictionary<string, string> commandResponsePairs = new Dictionary<string, string>()
            {
                { "SHOW CAL", "HID"},
            };

            try
            {
                var promptResponse = await LoggerSerialCommand(commandResponsePairs);
                if (!promptResponse.Item1)
                    throw new Exception($"Expected logger response not received. Actual response: {promptResponse.Item2}");

                result.Item1 = promptResponse.Item1;
                result.Item2 = promptResponse.Item2;

                result.Item2 = result.Item2.Substring(result.Item2.IndexOf("HID"));
                result.Item2 = result.Item2.Substring(result.Item2.IndexOf(":") + 1);
                result.Item2 = result.Item2.Remove(result.Item2.IndexOf("\r\nCR350")).Trim();

                List<UInt32> hidParsed = new List<UInt32>();
                string hidSubset = "";

                //example
                //Input HID: 3559430:540029003:50:2293798:17424:427026:117444632:35072:3540738030
                //Resulting Key: 7096
                foreach (char c in result.Item2)
                {
                    if (c == ':')
                    {
                        if (hidSubset != "") hidParsed.Add(Convert.ToUInt32(hidSubset));
                        hidSubset = "";
                    }
                    else
                    {
                        hidSubset += c;
                    }
                }
                hidParsed.Add(Convert.ToUInt32(hidSubset));

                UInt32 key = 1;
                foreach (var v in hidParsed)
                    key *= v;
                while (key >= 10000)
                    key = key / 2;
                if (Convert.ToBoolean(hidParsed[2] % 2))
                {
                    key = key / 3;
                }
                result.Item2 = key.ToString();


            }
            catch (Exception ex)
            {
                result.Item2 = ex.Message;
            }
            return result;

        }

        private async Task<(bool, string)> LoggerSerialCommand(Dictionary<string, string> commandResponsePairs, int readResponseWaitTime = 2, int recursionCount = 3)
        {
            bool result = false;
            string response = "";
            using (SerialPort sp = new SerialPort(comPort, Baudrate))
            {
                sp.DtrEnable = true;
                if (!sp.IsOpen) sp.Open();
                await sp.BaseStream.WriteAsync(Encoding.ASCII.GetBytes("\r"), 0, 1);
                await Task.Delay(1000);
                await sp.BaseStream.WriteAsync(Encoding.ASCII.GetBytes("\r"), 0, 1);
                await Task.Delay(1000);
                await sp.BaseStream.WriteAsync(Encoding.ASCII.GetBytes("\r"), 0, 1);
                await Task.Delay(1000);
                await sp.BaseStream.WriteAsync(Encoding.ASCII.GetBytes("\r"), 0, 1);
                await Task.Delay(1000);
                foreach (KeyValuePair<string, string> command in commandResponsePairs)
                {
                    await sp.BaseStream.WriteAsync(Encoding.ASCII.GetBytes($"{command.Key}\r"), 0, (command.Key.Length * sizeof(char)) / 2 + 1);
                    await Task.Delay(readResponseWaitTime * 1000);
                    DateTime wait = DateTime.Now;
                    bool breakForeach = false;
                    while (true)
                    {
                        response = sp.ReadExisting();
                        if (response.Contains(command.Value))
                        {
                            result = true;
                            //successfullResponses++;
                            break;
                        }
                        await Task.Delay(1000);
                        if (DateTime.Now.Subtract(wait).TotalSeconds > readResponseWaitTime)
                        {
                            result = false;
                            breakForeach = true;
                            response += $"\rError. Expected logger prompt ({command.Value}) not received. Logger response: {response}\r";
                            break;
                        }
                    }
                    if (breakForeach) //if timeout on the read response then don't attempt to send any more commands
                        break;
                }
            }
            if (!result && (recursionCount > 0))
            {
                recursionCount--;
                return await LoggerSerialCommand(commandResponsePairs, readResponseWaitTime, recursionCount);
            }
            return (result, response);
        }

        public async Task<(bool, string)> SetFactoryMode(string DevKey, string uid)
        {
            string tempUid = uid.Replace("-", "");
            (bool, string) result = (false, "");
            Dictionary<string, string> commandResponsePairs = new Dictionary<string, string>()
            {
                { "SET FACTORY MODE", "Enter password:"},
                { DevKey, "Factory mode set"},
                { $"SET UID {tempUid}", $"UID set to" },
                { "SHOW DEV INFO", tempUid}
            };
            try
            {
                var promptResponse = await LoggerSerialCommand(commandResponsePairs);
                if (!promptResponse.Item1)
                    throw new Exception($"Expected logger response not received ({commandResponsePairs.Values.ElementAt(0)} or {commandResponsePairs.Values.ElementAt(1)}). Actual response: {promptResponse.Item2}");

                result.Item1 = true;
                result.Item2 = promptResponse.Item2;
            }
            catch (Exception ex)
            {
                result.Item1 = false;
                result.Item2 = ex.Message;
            }
            return result;
        }

        private async Task PrintLabel(string modelNum, string serialNum, string uid)
        {
            //if (!Directory.Exists(SERVERPATH))
            //{
            //    Directory.CreateDirectory(SERVERPATH);

            //}

            await Task.Delay(500);
            String prnFilePath = @"C:\Users\mlund\Desktop\testPrint.txt";
            String fullPrinterPath = @"\\DKSY243\UidPrinter_ZD420_ZPL";// @"\\" + ServerName + @"\" + printerName;
            var createdFile = System.IO.File.Create(prnFilePath);
            createdFile.Close();
            System.IO.File.WriteAllText(prnFilePath,
                @"^XA~TA000~JSN^LT0^MNW^MTT^PON^PMN^LH0,0^JMA^PR4,4~SD15^JUS^LRN^CI0^XZ" +
                @"^XA" +
                @"^MMT" +
                @"^PW945" +
                @"^LS0" +
                @"^FT39,207^BQN,2,7" +
                @"^FH\^FDLA," + uid + "^FS" +
                @"^FT0,30^AAN,18,10^FH\^FB237,1,0,C,0^FD" + modelNum + " SN:" + serialNum + "^FS" +
                @"^FT0,210^AAN,18,10^FH\^FB237,1,0,C,0^FD" + uid + "^FS" +
                @"^PQ1,0,1,Y" +
                @"^XZ");

            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.RedirectStandardOutput = true;
            startInfo.UseShellExecute = false;
            startInfo.CreateNoWindow = true;
            startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Normal;
            startInfo.FileName = "cmd.exe";
            startInfo.Arguments = @"/c Copy /b " + prnFilePath + " " + fullPrinterPath;
            //startInfo.Arguments = @"/c Copy /b C:\Users\mlund\Desktop\testPrint.txt \\DKSY243\UidPrinter_ZD420_ZPL";
            process.StartInfo = startInfo;
            process.Start();
            string result = process.StandardOutput.ReadToEnd();

            //for (int i = 0; i < 1; i++)
            //{
            //process.Start();
            //process.WaitForExit();

            //}

            System.IO.File.Delete(prnFilePath);
        }

        private void serialTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Convert.ToInt32(e.KeyChar) == 13)
            {
                getUidButton_Click(sender, e);
            }
        }

        private void serialTextBox_MouseClick(object sender, MouseEventArgs e)
        {
            serialTextBox.SelectAll();
        }

        private void modelTextBox_MouseClick(object sender, MouseEventArgs e)
        {
            modelTextBox.SelectAll();
        }
    }

    struct API_ReturnData
    {
        public bool result;
        public string requestString;
        public string responseString;
    }


}
